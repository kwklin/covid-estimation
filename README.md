# COVID Estimation

As the Omicron wave hit Ontario, Covid daily cases started to increase and because of this, our trip to Montreal was canceled. To make up for our canceled trip, we came up with something fun to do, which is betting on daily Covid cases in Ontario. 

This project consists of 2 parts, including finding the best day as the starting date of the model, and predicting the number of covid cases on December 21st based on the model.

To find the best starting date, we would create a model for each possible starting date and use the one with the minimum MSE. After finding the best starting date, we make 3 predictions based on the model with the best starting date, the model that starts on the previous day, and the model that starts on the next day, so that we can have a bit more reference to make some manual judgments. 
